
// function to initialize the page
var init = function() {
	populateList();
	$('#formula-input').keypress(function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if ((code == 8)	// BS or delete
		|| ((33 <= code) && (code <= 40))){	// up/down, right/left, page up/down
			// just ignore (this is for firefox)
			return;
		}
		var currentText = $('#formula-input').val();
		switch (code) {
		case 13:	// enter
			e.preventDefault();
			if (!isSyntaxErrorExists) {
				stopFormatting();
				submitFormula();
			}
			break;
		case 32:	// space
			if (currentText.match(/ +$/)) {
				// there is already space appended, ignore the next one.
				e.preventDefault();
				break;
			}
		case 41:	// )
		case 42:	// *
		case 43:	// +
		case 46:	// .
		case 47:	// '/'
			if (currentText == '') {
				// those characters are not allowed at top
				e.preventDefault();
				break;
			}
		case 40:	// (
		case 45:	// -
		case 48:	// 0
		case 49:	// 1
		case 50:	// 2
		case 51:	// 3
		case 52:	// 4
		case 53:	// 5
		case 54:	// 6
		case 55:	// 7
		case 56:	// 8
		case 57:	// 9
			break;
		default:
			e.preventDefault();
			var chr = String.fromCharCode(code);
			showHint('<strong>Sorry!</strong> What you just typed in is not allowed.'
					+ ((code > 255) ? ' IME is active?' : ''));
			break;
		}
	});
	$('#formula-input').keyup(function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if ((code == 8)	// BS or delete
			|| (code == 32)) {	// space
			startFormatting();
		}
		else if (((48 <= code) && (code <= 57))	// 0 - 9 on main board
			|| ((96 <= code) && (code <= 105))	// 0 - 9 on numpad
			|| (code == 106) || (code == 107) || (code == 109) || (code == 111)	// * + - / on numpad
			|| (code == 56) || (code == 191)	// * / on main board
			|| (code == 61) || (code == 173)	// + - for firefox
			|| (code == 187) || (code == 189)	// + - for chrome
			|| (code == 190) || (code == 110))	// . on main board, . on numpad
		{
			var currentText = $('#formula-input').val();
			if (currentText != '') {
				// if there is an operator, make it surrounded by space.
				if (currentText.match(/([0-9]| |\))\+$/)) {
					$('#formula-input').val(currentText.replace(/( \+|\+)$/, ' + '));
				}
				else if (currentText.match(/([0-9]|\))-$/)) {
					$('#formula-input').val(currentText.replace(/\-$/, ' - '));
				}
				else if (currentText.match(/([0-9]| |\))\*$/)) {
					$('#formula-input').val(currentText.replace(/( \*|\*)$/, ' * '));
				}
				else if (currentText.match(/([0-9]| |\))\/$/)) {
					$('#formula-input').val(currentText.replace(/( \/|\/)$/, ' / '));
				}
				startFormatting();
			}
		}
	});
	$('#formula-input').focus();
}

var isFormatting = false;
var isSyntaxErrorExists = false;
var lastFormattingText = '';
var param = function(name, value) {	// parameter object to be sent to backend in the AJAX call.
	this.name = name;
	this.value = value;
}

var startFormatting = function() {
	setTimeout(formatFormula, 500);
}

var stopFormatting = function() {
	isFormatting = false;
	lastFormattingText = '';
}

var formatFormula = function() {
	if (isFormatting)
		return;
	var text = $('#formula-input').val();
	if (text == lastFormattingText)
		return;
	lastFormattingText = text;
	text = text.trim();
	if (text == '')
		return;
	if (text.match(/(\+|\-|\*|\/|\() *$/))	// ignore if the last character is either +, -, *, /, (
		return;
	isFormatting = true;
	showBusyImage();
	// creating the data object to be sent to backend
	var data = new Array();
	data[data.length] = new param('content', text);
	data[data.length] = new param('action', 'FORMAT');
	$.ajax({
		url : "/item",
		type : "POST",
		dataType : "json",
		data : data,
		success : function(resp) {
			isFormatting = false;
			hideBusyImage();
			lastFormattingText = '';
			if (resp != null) {
				var data = resp.data;
				if (data.redirect) {
					// User authentication expired or invalid request. Redirect to login.
					window.location.href = data.redirect;
				}
				else {
					var result = data.result;
					if (result == "true") {
						isSyntaxErrorExists = false;
						hideHint();
					}
					else {
						isSyntaxErrorExists = true;
						showFormatError(text, data.errorPosi, data.errorMsg)
					}
				}
			}
			else {
				showMessage("<strong>Sorry!</strong> Internal error happened.");
			}
		},
		error : function(xhr, status, error) {
			isFormatting = false;
			hideBusyImage();
			lastFormattingText = '';
			showMessage("<strong>Sorry!</strong> Internal error happened.");
		}
	});
}

var submitFormula = function() {
	var text = $('#formula-input').val();
	text = text.trim();
	if (text == '')
		return;
	if (text.match(/(\+|\-|\*|\/|\() *$/))	// ignore if the last character is either +, -, *, /, (
		return;
	$("#formula-input").attr("placeholder", "");
	showBusyImage();
	// creating the data object to be sent to backend
	var data = new Array();
	data[data.length] = new param('content', text.trim());
	data[data.length] = new param('action', 'SUBMIT');
	$.ajax({
		url : "/item",
		type : "POST",
		dataType : "json",
		data : data,
		success : function(resp) {
			if (resp != null) {
				var data = resp.data;
				if (data.redirect) {
					// User authentication expired or invalid request. Redirect to login.
					window.location.href = data.redirect;
				}
				else {
					var result = data.result;
					if (result == "true") {
						isSyntaxErrorExists = false;
						hideHint();
						hideMessage();
						$('#formula-input').val('');
						populateList();
					}
					else {
						isSyntaxErrorExists = true;
						showFormatError(text, data.errorPosi, data.errorMsg)
					}
				}
			}
			else {
				showMessage("<strong>Sorry!</strong> Internal error happened.");
			}
			hideBusyImage();
		},
		error : function(xhr, status, error) {
			showMessage("<strong>Sorry!</strong> Internal error happened.");
			hideBusyImage();
		}
	});
}

var populateList = function() {
	$.ajax({
		url : "/item",
		type : "GET",
		dataType : "json",
		success : function(resp) {
			if (resp) {
				// getting the data from the response object
				var data = resp.data;
				if (data.redirect) {
					// User authentication expired or invalid request. Redirect to login.
					window.location.href = data.redirect;
				}
				else {
					// creating the table content
					var htm = '';
					if (data.length > 0) {
						for ( var i = 0; i < data.length; i++) {
							htm += '<tr>';
							htm += '<td><blockquote>' + data[i].formula + ' = ' + data[i].result + '</blockquote></td>'
							htm += '</tr>';
						}
					} else {
						htm += '<tr><td>you have no calcords</td></tr>';
					}
					$('#calc-records-tbody').html(htm);
				}
			}
		},
		error : function(xhr, status, error) {
			showMessage("<strong>Sorry!</strong> Internal error happened.");
		}
	});
}

var showMessage = function(message) {
	$('#show-message').show().html(
			'<p><b>' + message + '</b></p>');
}

var hideMessage = function() {
	$('#show-message').hide();
}

var showHint = function(message) {
	$('#show-hint').show().html(
			'<p>' + message + '</p>');
}

var hideHint = function() {
	$('#show-hint').hide();
}

// charPosi: The position of character where syntax error resides, starting from 1.
var showFormatError = function(inputTxt, charPosi, message) {
	if (charPosi > 0) {
		var txtPre = inputTxt.substr(0, charPosi - 1);
		var txtErr = inputTxt.substr(charPosi - 1, 1);
		var txtPost = inputTxt.substr(charPosi);
		if (txtErr == ' ') {
			txtErr = "' '";
		}
		else if (txtErr == '') {
			txtErr = '?';
		}
		var inputTxt = '<p>' + txtPre + '<font color="red">' + txtErr + '</font>' + txtPost + '</p>';
		$('#show-hint').show().html(
				'<p>' + message + '</p>' + inputTxt);
	}
}

var showBusyImage = function() {
	$('#formula-input').addClass('formula-input-busy');
}

var hideBusyImage = function() {
	$('#formula-input').removeClass('formula-input-busy');
}
