<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.List" %>
<%@ page import="com.google.appengine.api.datastore.*" %>
<%@ page import="calcords.Util" %>

<%
	String userEmail = "";
	String userProvider = "";
	String userName = "Anonymous";
	String userPhotoUtl = "";
	String userID = request.getParameter("userid");
	Entity user = Util.getUserEntity(userID);
	if (user != null) {
		userEmail = (String)user.getProperty("email");
		userProvider = (String)user.getProperty("provider");
		userName = (String)user.getProperty("name");
		userPhotoUtl = (String)user.getProperty("photoUrl");
	}
%>		

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="free web calculator and recorder">
		<meta name="author" content="">
		<title>calcords - remember what you calculate</title>
		<link rel="icon" href="/favicon.ico">
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/calcords.css" rel="stylesheet">
	</head>
	<body>
		<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span> 
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">calcords</a>
				</div>
				<div id="navbar" class="collapse navbar-collapse">
					<ul class="nav navbar-nav navbar-right">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" style="padding: 0;"
								data-toggle="dropdown" role="button" aria-expanded="false">
								<img style="border-radius: 50%; width: 50px; height: 50px;" src="<%= userPhotoUtl %>"
									onerror="this.src='https:\/\/www.gstatic.com\/authtoolkit\/image\/profile-picture-small.png'; this.onerror=undefined;">
							</a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="/gitkit?mode=manageAccount">Setting</a></li>
								<li><a href="/logout">Logout</a></li>
							</ul>
						</li>
					</ul>
				</div><!--/.nav-collapse -->
			</div>
		</nav>

		<div class="container">
			<div id="formula-form">
				<input type="text" id="formula-input" autocomplete="off" maxlength="100" placeholder="try 1 * (2 + 3)" />
				<p>Your history of calculation</p>
				<div class="alert alert-info" role="alert" id="show-hint" style="display: none"></div>
				<div class="alert alert-warning" role="alert" id="show-message" style="display: none"></div>
			</div>
	
			<table class="table" id="calc-records">
				<tbody id="calc-records-tbody"></tbody>
			</table>

		</div><!-- /.container -->

		<script src='js/jquery-1.11.1.min.js'></script>
		<script src="js/bootstrap.min.js"></script>
		<script src='js/calcords.js'></script>
		<script type="text/javascript">
			$(window).load(function() {
				init();
			});
		</script>
	</body>
</html>
