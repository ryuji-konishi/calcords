<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
	response.addHeader("Access-Control-Allow-Origin", "*");
	response.addHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
	response.addHeader("Access-Control-Max-Age", "86400");
	if ("OPTIONS".equalsIgnoreCase(request.getMethod())) {
		response.addHeader("Access-Control-Allow-Credentials", "true");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!-- The HTML 4.01 Transitional DOCTYPE declaration-->
<!-- above set at the top of the file will set     -->
<!-- the browser's rendering engine into           -->
<!-- "Quirks Mode". Replacing this declaration     -->
<!-- with a "Standards Mode" doctype is supported, -->
<!-- but may lead to some differences in layout.   -->

<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>

<body>
	<div style="width: 256px; margin: auto">
		<img src="/image/logo_160x160.png"
			style="display: block; height: 160px; width: 160px; margin: auto">
	</div>
</body>
</html>
