java -jar ../../war/WEB-INF/lib/antlr-4.4-complete.jar  ./Expr.g4

# Add a package declaration at top of java files generated above.
sed -i '' '1 a\
package antlr;\
' ./ExprBaseListener.java

sed -i '' '1 a\
package antlr;\
' ./ExprLexer.java

sed -i '' '1 a\
package antlr;\
' ./ExprListener.java

sed -i '' '1 a\
package antlr;\
' ./ExprParser.java


