grammar Expr;
stat : expr ;
expr: expr ' ' (op = MULT | op = DIV) ' ' expr
    | expr ' ' (op = ADD | op = SUB) ' ' expr
    | NUM
    | '(' expr ')'
    ;

MULT: '*' ;
DIV : '/' ;
ADD : '+' ;
SUB : '-' ;
NUM : '-'*[0-9]+('.'[0-9]+)? ;
ErrorCharacter: '.' ;
//WS  : [ \t\n]+ -> skip ;
