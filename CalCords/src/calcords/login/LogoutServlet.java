package calcords.login;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import calcords.Util;

@SuppressWarnings("serial")
public class LogoutServlet extends HttpServlet {
	private static final Logger logger = Logger.getLogger(LogoutServlet.class.getName());
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		invalidateSession(request, response);
		Util.redirectToTopPage(request, response);
	}
	
	private void invalidateSession(HttpServletRequest request, HttpServletResponse response) {
	    response.setContentType("text/html");
	    Cookie[] cookies = request.getCookies();
	    // Delete all the cookies
	    if (cookies != null) {
	        for (int i = 0; i < cookies.length; i++) {
	            Cookie cookie = cookies[i];
	            cookies[i].setValue(null);
	            cookies[i].setMaxAge(0);
	            response.addCookie(cookie);
	        }
	    }
	}
}
