package calcords.login;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;

import calcords.Util;

import com.google.appengine.api.datastore.DatastoreFailureException;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.identitytoolkit.GitkitClientException;
import com.google.identitytoolkit.GitkitServerException;
import com.google.identitytoolkit.GitkitUser;

@SuppressWarnings("serial")
public class LoginServlet extends HttpServlet {
	private static final Logger logger = Logger.getLogger(LoginServlet.class.getName());
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// This check prevents the "/" handler from handling all requests by
		// default
		if (!"/".equals(request.getServletPath())) {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return;
		}

		try {
			GitkitUser gitkitUser = Util.gitkitUser(this, request);
			if (gitkitUser == null) {
				response.getWriter().print(
						new Scanner(new File("WEB-INF/templates/index.html"), "UTF-8").useDelimiter("\\A").next()
								.replaceAll("WELCOME_MESSAGE", "You are not logged in")
								.toString());
				response.setStatus(HttpServletResponse.SC_OK);
			}
			else {
				gitkitUser = Util.gitkitUserDetails(this, request, gitkitUser);
				logger.info(gitkitUser.getLocalId());
				storeUserInfo(gitkitUser);
				String id = gitkitUser.getLocalId();
				request.getRequestDispatcher("/WEB-INF/calcords.jsp?userid=" + id).forward(request, response);
			}

		} catch (FileNotFoundException | GitkitClientException | GitkitServerException | JSONException e) {
			e.printStackTrace();
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			response.getWriter().print(e.toString());
		} catch (DatastoreFailureException e) {
			logger.info(e.getMessage());
		}
	}
	
	private void storeUserInfo(GitkitUser gitkitUser)
	{
		String id = gitkitUser.getLocalId();
		Entity user = Util.getUserEntity(id);
		
		// Renew user entity properties.
		user.setProperty("email", gitkitUser.getEmail());
		user.setProperty("provider", gitkitUser.getCurrentProvider());
		user.setProperty("name", gitkitUser.getName());
		user.setProperty("photoUrl", gitkitUser.getPhotoUrl());

		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		datastore.put(user);
	}

}
