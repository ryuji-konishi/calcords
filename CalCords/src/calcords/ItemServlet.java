package calcords;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.identitytoolkit.GitkitClientException;
import com.google.identitytoolkit.GitkitServerException;
import com.google.identitytoolkit.GitkitUser;

@SuppressWarnings("serial")
public class ItemServlet extends HttpServlet {
	private static final Logger logger = Logger.getLogger(ItemServlet.class.getName());

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String userId = getCurrentUserId(req);
		if (userId == null) {
			Util.redirectToLogin(req, resp);
		}
		else {
			PrintWriter out = resp.getWriter();
			DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
			Key calcordsKey = KeyFactory.createKey("CalCords", userId);
			Query query = new Query("CalCord", calcordsKey).addSort("date", Query.SortDirection.DESCENDING);
			List<Entity> calcords = datastore.prepare(query).asList(FetchOptions.Builder.withLimit(10));
			out.println(Util.writeJSON(calcords));
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String userId = getCurrentUserId(req);
		if (userId == null) {
			Util.redirectToLogin(req, resp);
		}
		else {
			String action = req.getParameter("action");
			if (action.equalsIgnoreCase("delete")) {
				// this is yet to be implemented
				return;
			} else if (action.equalsIgnoreCase("submit")) {
				doSubmit(userId, req, resp);
			} else if (action.equalsIgnoreCase("format")) {
				doFormat(req, resp);
			}
		}
	}

	protected void doSubmit(String userId, HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String content = req.getParameter("content");
		if (content.isEmpty()) {
			logger.severe("Empty text is submitted. This can't happen. Check the front end javascript.");
		} else {
			Parser parser = new Parser();
			if (parser.parse(content) && parser.calculate()) {
				// store result into datastore
				Date date = new Date();
				Key calcordsKey = KeyFactory.createKey("CalCords", userId);
				Entity calcord = new Entity("CalCord", calcordsKey);
				calcord.setProperty("date", date);
				calcord.setProperty("user", userId);
				calcord.setProperty("formula", parser.getParsedFormula());
				calcord.setProperty("result", parser.getCalculatedResult());
				
				DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
				datastore.put(calcord);
				
				processResponse(true, parser, resp);
			} else {
				processResponse(false, parser, resp);
			}
		}
	}

	protected void doFormat(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String content = req.getParameter("content");
		if (content.isEmpty()) {
			logger.severe("Empty text is submitted. This can't happen. Check the front end javascript.");
		} else {
			Parser parser = new Parser();
			boolean result = parser.parse(content);
			processResponse(result, parser, resp);
		}
	}

	private void processResponse(boolean parseResult, Parser parser, HttpServletResponse resp)
			throws ServletException, IOException {
		PrintWriter out = resp.getWriter();
		Map<String, Object> values = new HashMap<String, Object>();
		if (parseResult) {
			values.put("result", true);
			values.put("formula", parser.getParsedFormula());
		} else {
			values.put("result", false);
			for (Parser.ParseSyntaxError err : parser.getParseSyntaxErrors()) {
				values.put("errorPosi", err.getCharPosition());
				values.put("errorMsg", err.getErrorMessage());
				// Break here and return only the 1st error. The following errors are not important. 
				break;
			}
		}
		out.println(Util.writeJSON(values));
	}
	
	private String getCurrentUserId(HttpServletRequest req)
			throws ServletException, IOException {
		String userId = null;
		try {
			GitkitUser gitkitUser = Util.gitkitUser(this, req);
			if (gitkitUser != null) {
				userId = gitkitUser.getLocalId();
			}
		}
		catch (GitkitClientException | GitkitServerException e) {
		}
		return userId;
	}
	
}
