package calcords;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.identitytoolkit.GitkitClient;
import com.google.identitytoolkit.GitkitClientException;
import com.google.identitytoolkit.GitkitServerException;
import com.google.identitytoolkit.GitkitUser;

public class Util {
	
	private static GitkitClient gitkitClient = null;
	private static String _urlLogin = "/gitkit?mode=select";
	private static String _urlTopPage = "/";
	
	public static Entity getUserEntity(String gitKitUserId)
	{
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Entity user = null;
		try {
			Key userKey = KeyFactory.createKey("Users", gitKitUserId);
			user = datastore.get(userKey);
		}
		catch (EntityNotFoundException e) {
			// User doesn't exist.
		}
		if (user == null) {
			// New User. Create new user entity.
			user = new Entity("Users", gitKitUserId);
		}
		return user;
	}

	public static GitkitUser gitkitUserDetails(HttpServlet servlet, HttpServletRequest req, GitkitUser base)
			throws FileNotFoundException, GitkitClientException, GitkitServerException {
		GitkitClient gitkitClient = gitkitClient(servlet);
		GitkitUser gitkitUser = gitkitClient.getUserByLocalId(base.getLocalId());
		return gitkitUser;
	}
	
	// This method is faster than gitkitUserDetails but return sparse user info.
	public static GitkitUser gitkitUser(HttpServlet servlet, HttpServletRequest req)
			throws FileNotFoundException, GitkitClientException, GitkitServerException {
		GitkitClient gitkitClient = gitkitClient(servlet);
		GitkitUser gitkitUser = gitkitClient.validateTokenInRequest(req);
		return gitkitUser;
	}
	
	public static GitkitClient gitkitClient(HttpServlet servlet) throws FileNotFoundException {
		if (gitkitClient == null) {
			ServletContext context = servlet.getServletContext();
			// Get the following values from 'gitkit-server-config.json' downloaded from Google Developers Console. 
			gitkitClient = GitkitClient.newBuilder().setGoogleClientId("142497602098-pt333uin52rpr0l509hdmkcu582srapm.apps.googleusercontent.com")
					.setServiceAccountEmail("142497602098-hopcroqoetf9dl3cmdl4mq0qcubqnls7@developer.gserviceaccount.com")
					.setKeyStream(new FileInputStream(context.getRealPath("/WEB-INF/calcrd-6f7f54886ab2.p12")))
					.setWidgetUrl("/gitkit").build();
		}
		return gitkitClient;
	}
	
	public static void redirectToTopPage(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		if ("XMLHttpRequest".equals(req.getHeader("X-Requested-With"))) {
			// This is an AJAX request. Return JSON object with redirect URL.
			PrintWriter out = resp.getWriter();
			out.println(Util.writeJSON_RedirectURL(_urlTopPage));
		}
		else {
			resp.sendRedirect(_urlTopPage);
		}
	}
	
	public static void redirectToLogin(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		if ("XMLHttpRequest".equals(req.getHeader("X-Requested-With"))) {
			// This is an AJAX request. Return JSON object with redirect URL.
			PrintWriter out = resp.getWriter();
			out.println(Util.writeJSON_RedirectURL(_urlLogin));
		}
		else {
			resp.sendRedirect(_urlLogin);
		}
	}

	public static String writeJSON_RedirectURL(String url) {
		StringBuilder sb = new StringBuilder();
		sb.append("{\"data\": ");
		sb.append("{\"redirect\" : \"" + url + "\"}");
		sb.append("}");
		return sb.toString();
	}

	public static String writeJSON(Iterable<Entity> entities) {
		StringBuilder sb = new StringBuilder();
		int i = 0;
		sb.append("{\"data\": [");
		for (Entity result : entities) {
			Map<String, Object> properties = result.getProperties();
			sb.append("{");
			if (result.getKey().getName() == null)
				sb.append("\"name\" : \"" + result.getKey().getId() + "\",");
			else
				sb.append("\"name\" : \"" + result.getKey().getName() + "\",");

			for (String key : properties.keySet()) {
				sb.append("\"" + key + "\" : \"" + properties.get(key) + "\",");
			}
			sb.deleteCharAt(sb.lastIndexOf(","));
			sb.append("},");
			i++;
		}
		if (i > 0) {
			sb.deleteCharAt(sb.lastIndexOf(","));
		}  
		sb.append("]}");
		return sb.toString();
	}

	public static String writeJSON(Map<String, Object> values) {
		StringBuilder sb = new StringBuilder();
		int i = 0;
		sb.append("{\"data\": {");
		for (String key : values.keySet()) {
			sb.append("\"" + key + "\" : \"" + values.get(key) + "\",");
			i++;
		}
		if (i > 0) {
			sb.deleteCharAt(sb.lastIndexOf(","));
		}  
		sb.append("}}");
		return sb.toString();
	}
}